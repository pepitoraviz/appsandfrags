const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var FragmentSchema = new Schema({
    process_name:String,
    application_name:String,
    favicon:String,
    window_title:String,
    time_opened:Date,
    time_spent:Date,
    url:String,
    domain:String,
    client:Schema.Types.ObjectId,
    agent:Schema.Types.ObjectId,
    employee:Schema.Types.ObjectId
})



module.exports = mongoose.model('Fragments',FragmentSchema)