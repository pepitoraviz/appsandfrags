var express = require('express')
var router = express.Router()
var controller = require('./applications.controller');
router.get('/',  controller.index);
router.get('/app_fragments', controller.fetchAppFragment)
router.post('/create',controller.create)
router.put('/update/:appId', controller.update)
router.patch('/update/:appId', controller.update)
router.delete('/delete/:appId', controller.delete)
module.exports = router;