var Applications = require('./applications.model')
var _ = require('lodash');


exports.index = function( req, res){
    Applications.getAll({},function(err, applicationObjs){
        if(err){ return handleErr(res, 500, err)}
        if(applicationObjs){
            return res.json(applicationObjs)
        }    

    })
}

exports.fetchAppFragment = function(req, res){
    Applications.fetchAppFragment(function(err, applicationObjs){
        if(err){ return handleErr(res, 500, err)}
        if(applicationObjs){
            return res.json(applicationObjs)
        }    

    })
}

exports.create = function(req, res){
    Applications.create(req.body,function(err, applicationObj){
        if(err)  return handleErr(res, 500 , 'unknown err')
        if(applicationObj) return res.status(201).json(applicationObj)
            
    })
}

exports.update = function(req, res){
    if(req.body._id) delete req.body._id
    Applications.findById(req.params.appId, function(err, applicationObj){
        if(err) return handleErr(res, 500 , 'unknown err')
        if(applicationObj){
            var tempApp = _.merge(applicationObj, req.body)
            tempApp.save(function(err, updatedAppObj){
                if(err) return handleErr(res, 500 , 'unknown err')
                if(updatedAppObj) res.status(201).json(updatedAppObj)
             })
        }
    })
}

exports.delete = function(req, res){
    Applications.remove({_id:req.params.appId}, function(err){
        if(err) return handleErr(res, 500 , 'unknown err')
        return res.status(201).json({remove:'success'})
    })
}
function handleErr(res, status, err){
    return res.status(status).json({err:err})
}