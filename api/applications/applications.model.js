const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const appTypeEnum = ["productive", "unproductive", "neutral", "idle"]
var ApplicationSchema = new Schema({
    application_name:String,
    application_type:{
        type:String,
        enum:appTypeEnum
    }
})

ApplicationSchema.statics.getAll = function(query,cb){
   return this.find(query,cb)
}

ApplicationSchema.statics.fetchAppFragment = function(cb){
    return this.aggregate([
        {
            $lookup:{
                from:'fragments',
                localField:'application_name',
                foreignField:"application_name"  ,
                as:"programs",
            },
        },  { "$addFields": { "total": { "$size": "$programs" } } },
      
    ],cb)   
}

module.exports = mongoose.model('Applications',ApplicationSchema);

