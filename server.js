
const express = require('express')
const app = express();
const path = require('path');
const server = require('http').createServer(app)
const bodyParser = require('body-parser');
const mongoose = require('mongoose')


mongoose.connect('mongodb://localhost/og-analytics', { db:{ safe:true }})
mongoose.connection.on('error', function(err) { 
    console.log('err: ', err);
    process.exit(-1);
})
app.use(bodyParser.urlencoded({extended:true}))
app.use('/static',express.static(path.join(__dirname, 'public')))
require('./routes')(app);
require('./seed');



server.listen(3000, function(){
    console.log("App is listening on port 3000")
})






app.get('/', function(req,res ){ 
    res.sendFile(__dirname + '/index.html')
})