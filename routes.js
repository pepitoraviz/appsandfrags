'use strict'


module.exports = function(app){
    app.use('/api/fragments', require('./api/fragments/'))
    app.use('/api/applications', require('./api/applications/'))
    
}